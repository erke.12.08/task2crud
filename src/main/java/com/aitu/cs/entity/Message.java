package com.aitu.cs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "request")
public class Message {
    @Id
    private long id;
    private long request_user_id;
    private long response_user_id;
    private long case_id;
    private long case_index_id;
    private String created_type;
    private String comment;
    private String status;
    private long timestamp;
    private long sharestart;
    private long sharefinish;
    private boolean favorite;
    private long update_timestamp;
    private long update_by;
    private String declinenote;
    private long company_unit_id;
    private long from_request_id;
}
