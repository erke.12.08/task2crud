package com.aitu.cs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tempfiles")
public class Tempfiles {
    @Id
    private long id;
    private String file_binary;
    private byte[] file_binary_byte;
}
