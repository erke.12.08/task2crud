package com.aitu.cs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "unit")
public class CompanyUnit {
    @Id
    private long id;
    private String name_ru;
    private String name_kz;
    private String name_en;
    private long parent_id;
    private int year;
    private int company_id;
    private String code_index;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;
}
