package com.aitu.cs.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "auth")
public class Auth {
    @Id
    private long id;
    private String username;
    private String email;
    private String password;
    private String role;
    private String forgot_password_key;
    private long forgot_password_key_timestamp;
    private long company_unit_id;
}
