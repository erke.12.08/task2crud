package com.aitu.cs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "acase")
public class ACase {
    @Id
    private long id;
    private String case_number;
    private String volume_number;
    private String title_kz;
    private String title_ru;
    private String  title_en;
    private long start_date;
    private long finish_date;
    private long pages_count;
    private boolean edstrue;
    private String eds;
    private boolean sendnag;
    private boolean delete;
    private boolean limitedaccess;
    private String hash;
    private int version;
    private String version_id;
    private boolean is_active_version;
    private String note;
    private long location_id;
    private long case_index_id;
    private long record_id;
    private long destroy_id;
    private long company_unit_id;
    private String case_address_block;
    private long date_input;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;






}
