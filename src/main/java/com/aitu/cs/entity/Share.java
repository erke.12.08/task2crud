package com.aitu.cs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "share")
public class Share {
    @Id
    private long id;
    private long request_id;
    private String note;
    private long sender_id;
    private long receiver_id;
    private long share_timestamp;


}
