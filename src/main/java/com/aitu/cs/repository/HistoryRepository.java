package com.aitu.cs.repository;

import com.aitu.cs.entity.History;
import org.springframework.data.repository.CrudRepository;

public interface HistoryRepository extends CrudRepository<History,Long> {
}
