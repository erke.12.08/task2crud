package com.aitu.cs.repository;

import com.aitu.cs.entity.CompanyUnit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyUnitRepository extends CrudRepository<CompanyUnit,Long> {
}
