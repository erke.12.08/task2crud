package com.aitu.cs.repository;

import com.aitu.cs.entity.SearchKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchKeyRepository extends CrudRepository<SearchKey,Long> {
}
