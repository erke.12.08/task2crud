package com.aitu.cs.repository;

import com.aitu.cs.entity.NomenclatureSummary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NomenclatureSummaryRepository extends CrudRepository<NomenclatureSummary,Long> {
}
