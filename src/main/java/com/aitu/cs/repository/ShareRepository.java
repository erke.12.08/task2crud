package com.aitu.cs.repository;

import com.aitu.cs.entity.Share;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShareRepository extends CrudRepository<Share,Long> {
}
