package com.aitu.cs.repository;

import com.aitu.cs.entity.FileRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRoutingRepository extends CrudRepository<FileRouting,Long> {
}
