package com.aitu.cs.repository;

import com.aitu.cs.entity.CatalogCase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatalogCaseRepository extends CrudRepository<CatalogCase,Long> {
}
