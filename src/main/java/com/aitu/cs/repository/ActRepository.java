package com.aitu.cs.repository;

import com.aitu.cs.entity.Act;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActRepository extends CrudRepository<Act,Long> {
}
