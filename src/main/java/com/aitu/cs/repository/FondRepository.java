package com.aitu.cs.repository;

import com.aitu.cs.entity.Fond;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FondRepository extends CrudRepository<Fond,Long> {
}
