package com.aitu.cs.repository;

import com.aitu.cs.entity.CaseIndex;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseIndexRepository extends CrudRepository <CaseIndex,Long> {
}
