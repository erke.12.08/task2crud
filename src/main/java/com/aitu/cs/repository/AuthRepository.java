package com.aitu.cs.repository;

import com.aitu.cs.entity.Auth;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthRepository extends CrudRepository<Auth, Long> {
    @Query(value = "select * from auth where id = 2", nativeQuery = true)
    Auth getAuth();


    List<Auth> findAll();

}
