package com.aitu.cs.repository;

import com.aitu.cs.entity.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends CrudRepository <Users, Long> {
}
