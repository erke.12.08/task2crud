package com.aitu.cs.repository;

import com.aitu.cs.entity.ACase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ACaseRepository extends CrudRepository<ACase, Long> {
}
