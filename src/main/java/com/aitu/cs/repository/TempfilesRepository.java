package com.aitu.cs.repository;

import com.aitu.cs.entity.Tempfiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TempfilesRepository extends CrudRepository<Tempfiles,Long> {
}
