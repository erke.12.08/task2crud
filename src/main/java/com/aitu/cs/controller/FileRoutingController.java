package com.aitu.cs.controller;

import com.aitu.cs.entity.FileRouting;
import com.aitu.cs.service.FileRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FileRoutingController {
    private final FileRoutingService fileRoutingService;

    public FileRoutingController(FileRoutingService fileRoutingService) {
        this.fileRoutingService = fileRoutingService;
    }

    @GetMapping(path="/api/route")
    public List<FileRouting> getAll() {
        return fileRoutingService.getAll();
    }
    @GetMapping("/api/route/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(fileRoutingService.getById(id));
    }

    @PostMapping("/api/route")
    public ResponseEntity<?> save(@RequestBody FileRouting fileRouting ) {
        return ResponseEntity.ok(fileRoutingService.create(fileRouting));
    }

    @PutMapping("/api/route")
    public ResponseEntity<?> update(@RequestBody FileRouting fileRouting ) {
        return ResponseEntity.ok(fileRoutingService.update(fileRouting));
    }

    @DeleteMapping("/api/route/{id}")
    public void delete(@PathVariable Long id) {
        fileRoutingService.delete(id);
    }
}
