package com.aitu.cs.controller;

import com.aitu.cs.entity.Act;
import com.aitu.cs.service.ActService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class ActController {
    public ActController(ActService actService) {
        this.actService = actService;
    }

    private final ActService actService;

    @GetMapping(path="/api/act")
    public List<Act> getAll() {
        return actService.getAll();
    }
    @GetMapping("/api/act/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(actService.getById(id));
    }

    @PostMapping("/api/act")
    public ResponseEntity<?> save(@RequestBody Act act) {
        return ResponseEntity.ok(actService.create(act));
    }

    @PutMapping("/api/act")
    public ResponseEntity<?> update(@RequestBody Act act ) {
        return ResponseEntity.ok(actService.update(act));
    }

    @DeleteMapping("/api/act/{id}")
    public void delete(@PathVariable Long id) {
        actService.delete(id);
    }

}
