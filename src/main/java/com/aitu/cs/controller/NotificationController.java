package com.aitu.cs.controller;

import com.aitu.cs.entity.Notification;
import com.aitu.cs.service.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NotificationController {
    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping(path="/api/not")
    public List<Notification> getAll() {
        return notificationService.getAll();
    }
    @GetMapping("/api/not/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(notificationService.getById(id));
    }

    @PostMapping("/api/not")
    public ResponseEntity<?> save(@RequestBody Notification notification ) {
        return ResponseEntity.ok(notificationService.create(notification));
    }

    @PutMapping("/api/not")
    public ResponseEntity<?> update(@RequestBody Notification notification ) {
        return ResponseEntity.ok(notificationService.update(notification));
    }

    @DeleteMapping("/api/not/{id}")
    public void delete(@PathVariable Long id) {
        notificationService.delete(id);
    }

}
