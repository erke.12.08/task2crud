package com.aitu.cs.controller;

import com.aitu.cs.entity.Catalog;
import com.aitu.cs.service.CatalogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CatalogController {
    private final CatalogService catalogService;

    public CatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @GetMapping(path="/api/catalog")
    public List<Catalog> getAll() {
        return catalogService.getAll();
    }
    @GetMapping("/api/catalog/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(catalogService.getById(id));
    }

    @PostMapping("/api/catalog")
    public ResponseEntity<?> save(@RequestBody Catalog catalog ) {

        return ResponseEntity.ok(catalogService.create(catalog));
    }

    @PutMapping("/api/catalog")
    public ResponseEntity<?> update(@RequestBody Catalog catalog ) {
        return ResponseEntity.ok(catalogService.update(catalog));
    }

    @DeleteMapping("/api/catalog/{id}")
    public void delete(@PathVariable Long id) {
        catalogService.delete(id);
    }
}
