package com.aitu.cs.controller;

import com.aitu.cs.entity.Record;
import com.aitu.cs.service.RecordService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RecordController {

    private final RecordService recordService;

    public RecordController(RecordService recordService) {
        this.recordService = recordService;
    }
    @GetMapping(path="/api/record")
    public List<Record> getAll() {
        return recordService.getAll();
    }
    @GetMapping("/api/record/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(recordService.getById(id));
    }

    @PostMapping("/api/record")
    public ResponseEntity<?> save(@RequestBody Record record ) {
        return ResponseEntity.ok(recordService.create(record));
    }

    @PutMapping("/api/record")
    public ResponseEntity<?> update(@RequestBody Record record ) {
        return ResponseEntity.ok(recordService.update(record));
    }

    @DeleteMapping("/api/record/{id}")
    public void delete(@PathVariable Long id) {
        recordService.delete(id);
    }
}
