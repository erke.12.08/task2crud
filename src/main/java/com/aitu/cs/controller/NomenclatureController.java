package com.aitu.cs.controller;

import com.aitu.cs.entity.Nomenclature;
import com.aitu.cs.service.NomenclatureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NomenclatureController {
    private final NomenclatureService nomenclatureService;

    public NomenclatureController(NomenclatureService nomenclatureService) {
        this.nomenclatureService = nomenclatureService;
    }

    @GetMapping(path="/api/nomenclature")
    public List<Nomenclature> getAll() {
        return nomenclatureService.getAll();
    }
    @GetMapping("/api/nomenclature/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(nomenclatureService.getById(id));
    }

    @PostMapping("/api/nomenclature")
    public ResponseEntity<?> save(@RequestBody Nomenclature nomenclature ) {
        return ResponseEntity.ok(nomenclatureService.create(nomenclature));
    }

    @PutMapping("/api/nomenclature")
    public ResponseEntity<?> update(@RequestBody Nomenclature nomenclature ) {
        return ResponseEntity.ok(nomenclatureService.update(nomenclature));
    }

    @DeleteMapping("/api//{id}")
    public void delete(@PathVariable Long id) {
        nomenclatureService.delete(id);
    }

}
