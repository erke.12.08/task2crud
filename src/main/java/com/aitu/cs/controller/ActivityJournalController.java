package com.aitu.cs.controller;

import com.aitu.cs.entity.ActivityJournal;
import com.aitu.cs.service.ActivityJournalService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ActivityJournalController {
    private final ActivityJournalService activityJournalService;

    public ActivityJournalController(ActivityJournalService activityJournalService) {
        this.activityJournalService = activityJournalService;
    }


    @GetMapping(path="/api/activity")
    public List<ActivityJournal> getAll() {
        return activityJournalService.getAll();
    }
    @GetMapping("/api/activity/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(activityJournalService.getById(id));
    }

    @PostMapping("/api/activity")
    public ResponseEntity<?> save(@RequestBody ActivityJournal activityJournal ) {
        return ResponseEntity.ok(activityJournalService.create(activityJournal));
    }

    @PutMapping("/api/activity")
    public ResponseEntity<?> update(@RequestBody ActivityJournal activityJournal ) {
        return ResponseEntity.ok(activityJournalService.update(activityJournal));
    }

    @DeleteMapping("/api/activity/{id}")
    public void delete(@PathVariable Long id) {
        activityJournalService.delete(id);
    }
}
