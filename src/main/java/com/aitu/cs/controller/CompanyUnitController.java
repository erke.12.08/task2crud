package com.aitu.cs.controller;

import com.aitu.cs.entity.CompanyUnit;
import com.aitu.cs.service.CompanyUnitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CompanyUnitController {

    private final CompanyUnitService companyUnitService;

    public CompanyUnitController(CompanyUnitService companyUnitService) {
        this.companyUnitService = companyUnitService;
    }

    @GetMapping(path="/api/unit")
    public List<CompanyUnit> getAll() {
        return companyUnitService.getAll();
    }
    @GetMapping("/api/unit/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(companyUnitService.getById(id));
    }

    @PostMapping("/api/unit")
    public ResponseEntity<?> save(@RequestBody CompanyUnit companyUnit ) {
        return ResponseEntity.ok(companyUnitService.create(companyUnit));
    }

    @PutMapping("/api/unit")
    public ResponseEntity<?> update(@RequestBody CompanyUnit companyUnit) {
        return ResponseEntity.ok(companyUnitService.update(companyUnit));
    }

    @DeleteMapping("/api/unit/{id}")
    public void delete(@PathVariable Long id) {
        companyUnitService.delete(id);
    }
}
