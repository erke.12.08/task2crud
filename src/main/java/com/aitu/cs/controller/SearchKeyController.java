package com.aitu.cs.controller;

import com.aitu.cs.entity.SearchKey;
import com.aitu.cs.service.SearchKeyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SearchKeyController {
    private final SearchKeyService searchKeyService;

    public SearchKeyController(SearchKeyService searchKeyService) {
        this.searchKeyService = searchKeyService;
    }

    @GetMapping(path="/api/key")
    public List<SearchKey> getAll() {
        return searchKeyService.getAll();
    }
    @GetMapping("/api/key/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(searchKeyService.getById(id));
    }

    @PostMapping("/api/key")
    public ResponseEntity<?> save(@RequestBody SearchKey searchKey ) {
        return ResponseEntity.ok(searchKeyService.create(searchKey));
    }

    @PutMapping("/api/key")
    public ResponseEntity<?> update(@RequestBody SearchKey searchKey ) {
        return ResponseEntity.ok(searchKeyService.update(searchKey));
    }

    @DeleteMapping("/api/key/{id}")
    public void delete(@PathVariable Long id) {
        searchKeyService.delete(id);
    }

}
