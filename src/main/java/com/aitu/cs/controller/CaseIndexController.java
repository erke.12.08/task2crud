package com.aitu.cs.controller;


import com.aitu.cs.entity.CaseIndex;
import com.aitu.cs.service.CaseIndexService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CaseIndexController {
    private final CaseIndexService caseIndexService;

    public CaseIndexController(CaseIndexService caseIndexService) {
        this.caseIndexService = caseIndexService;
    }
    @GetMapping("/api/index/{id}")
    public ResponseEntity<?> getCaseIndex(@PathVariable Long id) {
        return ResponseEntity.ok(caseIndexService.getById(id));
    }

    @GetMapping(path="/api/index")
    public List<CaseIndex> getAllCaseIndex() {
        return caseIndexService.getAll();
    }

    @PostMapping("/api/index")
    public ResponseEntity<?> saveCaseIndex(@RequestBody CaseIndex caseIndex) {
        return ResponseEntity.ok(caseIndexService.create(caseIndex));
    }

    @PutMapping("/api/index")
    public ResponseEntity<?> update(@RequestBody CaseIndex caseIndex) {
        return ResponseEntity.ok(caseIndexService.update(caseIndex));
    }

    @DeleteMapping("/api/index/{id}")
    public void deleteCaseIndex(@PathVariable Long id) {
        caseIndexService.delete(id);
    }

}
