package com.aitu.cs.controller;

import com.aitu.cs.entity.File;
import com.aitu.cs.service.FileService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FileController {

    private final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }
    @GetMapping(path="/api/file")
    public List<File> getAll() {
        return fileService.getAll();
    }
    @GetMapping("/api/file/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(fileService.getById(id));
    }

    @PostMapping("/api/file")
    public ResponseEntity<?> save(@RequestBody File file ) {
        return ResponseEntity.ok(fileService.create(file));
    }

    @PutMapping("/api/file")
    public ResponseEntity<?> update(@RequestBody File file) {
        return ResponseEntity.ok(fileService.update(file));
    }

    @DeleteMapping("/api/file/{id}")
    public void delete(@PathVariable Long id) {
        fileService.delete(id);
    }


}

