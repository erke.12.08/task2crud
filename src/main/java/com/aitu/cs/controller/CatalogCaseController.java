package com.aitu.cs.controller;

import com.aitu.cs.entity.CatalogCase;
import com.aitu.cs.service.CatalogCaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CatalogCaseController {
    private final CatalogCaseService catalogCaseService;

    public CatalogCaseController(CatalogCaseService catalogCaseService) {
        this.catalogCaseService = catalogCaseService;
    }

    @GetMapping(path="/api/catalogcase")
    public List<CatalogCase> getAll() {
        return catalogCaseService.getAll();
    }
    @GetMapping("/api/catalogcase/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(catalogCaseService.getById(id));
    }

    @PostMapping("/api/catalogcase")
    public ResponseEntity<?> save(@RequestBody CatalogCase catalogCase ) {
        return ResponseEntity.ok(catalogCaseService.create(catalogCase));
    }

    @PutMapping("/api/catalogcase")
    public ResponseEntity<?> update(@RequestBody CatalogCase catalogCase ) {
        return ResponseEntity.ok(catalogCaseService.update(catalogCase));
    }

    @DeleteMapping("/api/catalogcase/{id}")
    public void delete(@PathVariable Long id) {
        catalogCaseService.delete(id);
    }

}
