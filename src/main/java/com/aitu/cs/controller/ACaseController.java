package com.aitu.cs.controller;


import com.aitu.cs.entity.ACase;
import com.aitu.cs.service.ACaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ACaseController {
    private final ACaseService aCaseService;

    public ACaseController(ACaseService aCaseService) {
        this.aCaseService = aCaseService;
    }
    @GetMapping("/api/acase/{id}")
    public ResponseEntity<?> getAuth(@PathVariable Long id) {
        return ResponseEntity.ok(aCaseService.getById(id));
    }

    @GetMapping(path="/api/acase")
    public List<ACase> getAllACase() {
        return aCaseService.getAll();
    }

    @PostMapping("/api/acase")
    public ResponseEntity<?> saveACase(@RequestBody ACase aCase) {
        return ResponseEntity.ok(aCaseService.create(aCase));
    }

    @PutMapping("/api/acase")
    public ResponseEntity<?> update(@RequestBody ACase aCase) {
        return ResponseEntity.ok(aCaseService.update(aCase));
    }

    @DeleteMapping("/api/acase/{id}")
    public void deleteACase(@PathVariable Long id) {
        aCaseService.delete(id);
    }
}
