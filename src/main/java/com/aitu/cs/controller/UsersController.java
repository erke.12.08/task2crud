package com.aitu.cs.controller;


import com.aitu.cs.entity.Users;
import com.aitu.cs.service.UsersService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UsersController {
    private final UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/api/users/{id}")
    public ResponseEntity<?> getUsers(@PathVariable Long id) {
        return ResponseEntity.ok(usersService.getById(id));
    }

    @GetMapping(path="/api/users")
    public List<Users> getAllUsers() {
        return usersService.getAll();
    }

    @PostMapping("/api/users")
    public ResponseEntity<?> saveUsers(@RequestBody Users users) {
        return ResponseEntity.ok(usersService.create(users));
    }

    @PutMapping("/api/users")
    public ResponseEntity<?> update(@RequestBody Users users) {
        return ResponseEntity.ok(usersService.update(users));
    }

    @DeleteMapping("/api/users/{id}")
    public void deleteUsers(@PathVariable Long id) {
        usersService.delete(id);
    }
}
