package com.aitu.cs.controller;


import com.aitu.cs.entity.Auth;
import com.aitu.cs.service.AuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AuthController {
    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }
    @GetMapping("/api/auth/{id}")
    public ResponseEntity<?> getAuth(@PathVariable Long id) {
        return ResponseEntity.ok(authService.getById(id));
    }

    @GetMapping(path="/api/auth")
    public List<Auth> getAllAuto() {
        return authService.getAll();
    }

    @PostMapping("/api/auth")
    public ResponseEntity<?> saveAuth(@RequestBody Auth auth) {
        return ResponseEntity.ok(authService.create(auth));
    }

    @PutMapping("/api/auth")
    public ResponseEntity<?> update(@RequestBody Auth auth) {
        return ResponseEntity.ok(authService.update(auth));
    }

    @DeleteMapping("/api/auth/{id}")
    public void deleteAuth(@PathVariable Long id) {
        authService.delete(id);
    }
}
