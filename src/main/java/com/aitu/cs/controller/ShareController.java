package com.aitu.cs.controller;


import com.aitu.cs.entity.Share;
import com.aitu.cs.service.ShareService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ShareController {

    public ShareController(ShareService shareService) {
        this.shareService = shareService;
    }

    private final ShareService shareService;

    @GetMapping("/api/share/{id}")
    public ResponseEntity<?> getShareById(@PathVariable Long id) {
        return ResponseEntity.ok(shareService.getById(id));
    }

    @GetMapping(path="/api/share")
    public List<Share> getAllShare() {
        return shareService.getAll();
    }

    @PostMapping("/api/share")
    public ResponseEntity<?> saveShare(@RequestBody Share share) {
        return ResponseEntity.ok(shareService.create(share));
    }

    @PutMapping("/api/share")
    public ResponseEntity<?> updateShare(@RequestBody Share share) {
        return ResponseEntity.ok(shareService.update(share));
    }

    @DeleteMapping("/api/share/{id}")
    public void deleteMessage(@PathVariable Long id) {
        shareService.delete(id);
    }

}
