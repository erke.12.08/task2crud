package com.aitu.cs.controller;

import com.aitu.cs.entity.Location;
import com.aitu.cs.service.LocationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LocationController {
    private final LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping(path="/api/location")
    public List<Location> getAll() {
        return locationService.getAll();
    }
    @GetMapping("/api/location/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(locationService.getById(id));
    }

    @PostMapping("/api/location")
    public ResponseEntity<?> save(@RequestBody Location location ) {
        return ResponseEntity.ok(locationService.create(location));
    }

    @PutMapping("/api/location")
    public ResponseEntity<?> update(@RequestBody Location location ) {
        return ResponseEntity.ok(locationService.update(location));
    }

    @DeleteMapping("/api/location/{id}")
    public void delete(@PathVariable Long id) {
        locationService.delete(id);
    }
}
