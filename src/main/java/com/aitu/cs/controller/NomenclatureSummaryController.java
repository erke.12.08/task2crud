package com.aitu.cs.controller;

import com.aitu.cs.entity.NomenclatureSummary;
import com.aitu.cs.service.NomenclatureSummaryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NomenclatureSummaryController {

    private final NomenclatureSummaryService nomenclatureSummaryService;

    public NomenclatureSummaryController(NomenclatureSummaryService nomenclatureSummaryService) {
        this.nomenclatureSummaryService = nomenclatureSummaryService;
    }

    @GetMapping(path="/api/sum")
    public List<NomenclatureSummary> getAll() {
        return nomenclatureSummaryService.getAll();
    }
    @GetMapping("/api/sum/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(nomenclatureSummaryService.getById(id));
    }

    @PostMapping("/api/sum")
    public ResponseEntity<?> save(@RequestBody NomenclatureSummary nomenclatureSummary) {
        return ResponseEntity.ok(nomenclatureSummaryService.create(nomenclatureSummary));
    }

    @PutMapping("/api/sum")
    public ResponseEntity<?> update(@RequestBody NomenclatureSummary nomenclatureSummary ) {
        return ResponseEntity.ok(nomenclatureSummaryService.update(nomenclatureSummary));
    }

    @DeleteMapping("/api/sum/{id}")
    public void delete(@PathVariable Long id) {
        nomenclatureSummaryService.delete(id);
    }
}
