package com.aitu.cs.controller;

import com.aitu.cs.entity.SearchKeyRouting;
import com.aitu.cs.service.SearchKeyRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SearchKeyRoutingController {

    private final SearchKeyRoutingService searchKeyRoutingService;

    public SearchKeyRoutingController(SearchKeyRoutingService searchKeyRoutingService) {
        this.searchKeyRoutingService = searchKeyRoutingService;
    }

    @GetMapping(path="/api/keyrouting")
    public List<SearchKeyRouting> getAll() {
        return searchKeyRoutingService.getAll();
    }
    @GetMapping("/api/keyrouting/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(searchKeyRoutingService.getById(id));
    }

    @PostMapping("/api/keyrouting")
    public ResponseEntity<?> save(@RequestBody SearchKeyRouting searchKeyRouting ) {
        return ResponseEntity.ok(searchKeyRoutingService.create(searchKeyRouting));
    }

    @PutMapping("/api/keyrouting")
    public ResponseEntity<?> update(@RequestBody SearchKeyRouting searchKeyRouting) {
        return ResponseEntity.ok(searchKeyRoutingService.update(searchKeyRouting));
    }

    @DeleteMapping("/api/keyrouting/{id}")
    public void delete(@PathVariable Long id) {
        searchKeyRoutingService.delete(id);
    }

}
