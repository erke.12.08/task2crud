package com.aitu.cs.controller;


import com.aitu.cs.entity.History;
import com.aitu.cs.service.HistoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class HistoryController {
    private final HistoryService historyService;

    public HistoryController(HistoryService historyService) {
        this.historyService = historyService;
    }

    @GetMapping("/api/history/{id}")
    public ResponseEntity<?> getHistory(@PathVariable Long id) {
        return ResponseEntity.ok(historyService.getById(id));
    }

    @GetMapping(path="/api/history")
    public List<History> getAllHistory() {
        return historyService.getAll();
    }

    @PostMapping("/api/history")
    public ResponseEntity<?> saveHistory(@RequestBody History history) {
        return ResponseEntity.ok(historyService.create(history));
    }

    @PutMapping("/api/history")
    public ResponseEntity<?> update(@RequestBody History history) {
        return ResponseEntity.ok(historyService.update(history));
    }

    @DeleteMapping("/api/history/{id}")
    public void deleteHistory(@PathVariable Long id) {
        historyService.delete(id);
    }
}
