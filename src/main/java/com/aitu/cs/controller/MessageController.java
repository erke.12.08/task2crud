package com.aitu.cs.controller;


import com.aitu.cs.entity.Message;
import com.aitu.cs.service.MessageService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MessageController {
    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/api/message/{id}")
    public ResponseEntity<?> getMessageById(@PathVariable Long id) {
        return ResponseEntity.ok(messageService.getById(id));
    }

    @GetMapping(path="/api/message")
    public List<Message> getAllMessage() {
        return messageService.getAll();
    }

    @PostMapping("/api/message")
    public ResponseEntity<?> saveMessage(@RequestBody Message message) {
        return ResponseEntity.ok(messageService.create(message));
    }

    @PutMapping("/api/message")
    public ResponseEntity<?> updateMessage(@RequestBody Message message) {
        return ResponseEntity.ok(messageService.update(message));
    }

    @DeleteMapping("/api/message/{id}")
    public void deleteMessage(@PathVariable Long id) {
        messageService.delete(id);
    }
}
