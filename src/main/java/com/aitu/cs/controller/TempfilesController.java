package com.aitu.cs.controller;

import com.aitu.cs.entity.Tempfiles;
import com.aitu.cs.service.TempfilesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TempfilesController {
    private final TempfilesService tempfilesService;

    public TempfilesController(TempfilesService tempfilesService) {
        this.tempfilesService = tempfilesService;
    }

    @GetMapping(path="/api/tempfiles")
    public List<Tempfiles> getAll() {
        return tempfilesService.getAll();
    }
    @GetMapping("/api/tempfiles/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(tempfilesService.getById(id));
    }

    @PostMapping("/api/tempfiles")
    public ResponseEntity<?> save(@RequestBody Tempfiles tempfiles) {
        return ResponseEntity.ok(tempfilesService.create(tempfiles));
    }

    @PutMapping("/api/tempfiles")
    public ResponseEntity<?> update(@RequestBody Tempfiles tempfiles) {
        return ResponseEntity.ok(tempfilesService.update(tempfiles));
    }

    @DeleteMapping("/api/tempfiles/{id}")
    public void delete(@PathVariable Long id) {
        tempfilesService.delete(id);
    }
}
