package com.aitu.cs.controller;

import com.aitu.cs.entity.Company;
import com.aitu.cs.service.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CompanyController {
    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }
    @GetMapping(path="/api/company")
    public List<Company> getAll() {
        return companyService.getAll();
    }
    @GetMapping("/api/company/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(companyService.getById(id));
    }

    @PostMapping("/api/company")
    public ResponseEntity<?> save(@RequestBody Company company) {
        return ResponseEntity.ok(companyService.create(company));
    }

    @PutMapping("/api/company")
    public ResponseEntity<?> update(@RequestBody Company company) {
        return ResponseEntity.ok(companyService.update(company));
    }

    @DeleteMapping("/api/company/{id}")
    public void delete(@PathVariable Long id) {
        companyService.delete(id);
    }

}
