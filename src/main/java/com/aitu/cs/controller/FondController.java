package com.aitu.cs.controller;

import com.aitu.cs.entity.Fond;
import com.aitu.cs.service.FondService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FondController {

    private final FondService fondService;

    public FondController(FondService fondService) {
        this.fondService = fondService;
    }

    @GetMapping(path="/api/fond")
    public List<Fond> getAll() {
        return fondService.getAll();
    }
    @GetMapping("/api/fond/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(fondService.getById(id));
    }

    @PostMapping("/api/fond")
    public ResponseEntity<?> save(@RequestBody Fond fond ) {
        return ResponseEntity.ok(fondService.create(fond));
    }

    @PutMapping("/api/fond")
    public ResponseEntity<?> update(@RequestBody Fond fond ) {
        return ResponseEntity.ok(fondService.update(fond));
    }

    @DeleteMapping("/api/fond/{id}")
    public void delete(@PathVariable Long id) {
        fondService.delete(id);
    }

}
