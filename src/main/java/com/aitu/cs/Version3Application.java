package com.aitu.cs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Version3Application {

    public static void main(String[] args) {
        SpringApplication.run(Version3Application.class, args);
    }

}
