package com.aitu.cs.service;

import com.aitu.cs.entity.SearchKeyRouting;
import com.aitu.cs.repository.SearchKeyRoutingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchKeyRoutingService {
    private final SearchKeyRoutingRepository searchKeyRoutingRepository;

    public SearchKeyRoutingService(SearchKeyRoutingRepository searchKeyRoutingRepository) {
        this.searchKeyRoutingRepository = searchKeyRoutingRepository;
    }

    public List<SearchKeyRouting> getAll() {
        return (List<SearchKeyRouting>) searchKeyRoutingRepository.findAll();
    }

    public SearchKeyRouting getById(Long searchKeyRouting_id) {
        return searchKeyRoutingRepository.findById(searchKeyRouting_id).orElse(null);
    }

    public SearchKeyRouting create(SearchKeyRouting searchKeyRouting) {
        return searchKeyRoutingRepository.save(searchKeyRouting);
    }

    public SearchKeyRouting update(SearchKeyRouting searchKeyRouting) {
        return searchKeyRoutingRepository.save(searchKeyRouting);
    }

    public void delete(Long searchKeyRouting_id) {
        searchKeyRoutingRepository.deleteById(searchKeyRouting_id);
    }

}
