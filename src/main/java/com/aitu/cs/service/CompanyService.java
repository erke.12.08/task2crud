package com.aitu.cs.service;

import com.aitu.cs.entity.Company;
import com.aitu.cs.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {
    public  final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> getAll() {
        return (List<Company>) companyRepository.findAll();
    }

    public Company getById(Long company_id) {
        return companyRepository.findById(company_id).orElse(null);
    }

    public Company create(Company company) {
        return  companyRepository.save(company);
    }

    public Company update(Company company) {
        return companyRepository.save(company);
    }

    public void delete(Long _id) {
        companyRepository.deleteById(_id);
    }
}
