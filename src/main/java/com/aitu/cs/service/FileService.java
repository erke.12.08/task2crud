package com.aitu.cs.service;


import com.aitu.cs.entity.File;
import com.aitu.cs.repository.FileRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileService {
    public final FileRepository fileRepository;

    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public List<File> getAll() {
        return (List<File>) fileRepository.findAll();
    }

    public File getById(Long file_id) {
        return fileRepository.findById(file_id).orElse(null);
    }

    public File create(File file) {
        return fileRepository.save(file);
    }

    public File update(File file) {
        return fileRepository.save(file);
    }

    public void delete(Long _id) {
        fileRepository.deleteById(_id);
    }


}
