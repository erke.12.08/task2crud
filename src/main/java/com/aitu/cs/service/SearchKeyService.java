package com.aitu.cs.service;

import com.aitu.cs.entity.SearchKey;
import com.aitu.cs.repository.SearchKeyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchKeyService {

    private final SearchKeyRepository searchKeyRepository;

    public SearchKeyService(SearchKeyRepository searchKeyRepository) {
        this.searchKeyRepository = searchKeyRepository;
    }
    public List<SearchKey> getAll() {
        return (List<SearchKey>) searchKeyRepository.findAll();
    }

    public SearchKey getById(Long searchKey_id) {
        return searchKeyRepository.findById(searchKey_id).orElse(null);
    }

    public SearchKey create(SearchKey searchKey) {
        return searchKeyRepository.save(searchKey);
    }

    public SearchKey  update(SearchKey searchKey) {
        return searchKeyRepository.save(searchKey);
    }

    public void delete(Long searchKey_id) {
        searchKeyRepository.deleteById(searchKey_id);
    }

}
