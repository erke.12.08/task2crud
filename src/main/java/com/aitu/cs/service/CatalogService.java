package com.aitu.cs.service;

import com.aitu.cs.entity.Catalog;
import com.aitu.cs.repository.CatalogRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogService {

    private final CatalogRepository catalogRepository;

    public CatalogService(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }

    public List<Catalog> getAll() {
        return (List<Catalog>) catalogRepository.findAll();
    }

    public Catalog  getById(Long catalog_id) {
        return catalogRepository.findById(catalog_id).orElse(null);
    }

    public Catalog create(Catalog catalog) {
        return catalogRepository.save(catalog);
    }

    public Catalog update(Catalog catalog) {
        return catalogRepository.save(catalog);
    }

    public void delete(Long catalog_id) {
        catalogRepository.deleteById(catalog_id);
    }

}
