package com.aitu.cs.service;

import com.aitu.cs.entity.Location;
import com.aitu.cs.repository.LocationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationService {
    private final LocationRepository locationRepository;

    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public List<Location> getAll() {
        return (List<Location>) locationRepository.findAll();
    }

    public Location getById(Long location_id) {
        return locationRepository.findById(location_id).orElse(null);
    }

    public Location create(Location location) {
        return locationRepository.save(location);
    }

    public Location update(Location location) {
        return locationRepository.save(location);
    }

    public void delete(Long location_id) {
        locationRepository.deleteById(location_id);
    }
}
