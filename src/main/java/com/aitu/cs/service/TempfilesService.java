package com.aitu.cs.service;

import com.aitu.cs.entity.Tempfiles;
import com.aitu.cs.repository.TempfilesRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TempfilesService {
    private final TempfilesRepository tempfilesRepository;

    public TempfilesService(TempfilesRepository tempfilesRepository) {
        this.tempfilesRepository = tempfilesRepository;
    }
    public List<Tempfiles> getAll() {
        return (List<Tempfiles>) tempfilesRepository.findAll();
    }

    public Tempfiles getById(Long tempfiles_id) {
        return tempfilesRepository.findById(tempfiles_id).orElse(null);
    }

    public Tempfiles create(Tempfiles tempfiles) {
        return tempfilesRepository.save(tempfiles);
    }

    public Tempfiles update(Tempfiles tempfiles) {
        return tempfilesRepository.save(tempfiles);
    }

    public void delete(Long tempfiles_id) {
        tempfilesRepository.deleteById(tempfiles_id);
    }



}
