package com.aitu.cs.service;

import com.aitu.cs.entity.Auth;
import com.aitu.cs.repository.AuthRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthService {
    public final AuthRepository authRepository;

    public AuthService(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }


    public List<Auth> getAll() {
        return (List<Auth>) authRepository.findAll();
    }

    public Auth getById(Long auth_id) {
        return authRepository.findById(auth_id).orElse(null);
    }

    public Auth create(Auth auth) {
        return authRepository.save(auth);
    }

    public Auth update(Auth auth) {
        return authRepository.save(auth);
    }

    public void delete(Long auth_id) {
        authRepository.deleteById(auth_id);
    }
}
