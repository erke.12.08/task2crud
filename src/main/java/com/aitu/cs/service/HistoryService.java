package com.aitu.cs.service;

import com.aitu.cs.entity.History;
import com.aitu.cs.repository.HistoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HistoryService {
    public final HistoryRepository historyRepository;

    public HistoryService(HistoryRepository historyRepository) {
        this.historyRepository = historyRepository;
    }


    public List<History> getAll() {
        return (List<History>) historyRepository.findAll();
    }

    public History getById(Long history_id) {
        return historyRepository.findById(history_id).orElse(null);
    }

    public History create(History history) {
        return historyRepository.save(history);
    }

    public History  update(History history) {
        return historyRepository.save(history);
    }

    public void delete(Long history_id) {
        historyRepository.deleteById(history_id);
    }

}
