package com.aitu.cs.service;


import com.aitu.cs.entity.Users;
import com.aitu.cs.repository.UsersRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersService {

    public final UsersRepository usersRepository;

    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }


    public List<Users> getAll() {
        return (List<Users>) usersRepository.findAll();
    }

    public Users getById(Long auth_id) {
        return usersRepository.findById(auth_id).orElse(null);
    }

    public Users create(Users auth) {
        return usersRepository.save(auth);
    }

    public Users update(Users auth) {
        return usersRepository.save(auth);
    }

    public void delete(Long auth_id) {
        usersRepository.deleteById(auth_id);
    }
}
