package com.aitu.cs.service;

import com.aitu.cs.entity.Record;
import com.aitu.cs.repository.RecordRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecordService {

    public final RecordRepository recordRepository;

    public RecordService(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    public List<Record> getAll() {
        return (List<Record>) recordRepository.findAll();
    }

    public Record  getById(Long record_id) {
        return recordRepository.findById(record_id).orElse(null);
    }

    public Record  create(Record record) {
        return recordRepository.save(record);
    }

    public Record  update( Record record) {
        return recordRepository.save(record);
    }

    public void delete(Long record_id) {
        recordRepository.deleteById(record_id);
    }



}
