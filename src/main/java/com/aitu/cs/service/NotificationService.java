package com.aitu.cs.service;

import com.aitu.cs.entity.Notification;
import com.aitu.cs.repository.NotificationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationService {
    private final NotificationRepository notificationRepository;

    public NotificationService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }
    public List<Notification> getAll() {
        return (List<Notification>) notificationRepository.findAll();
    }

    public Notification  getById(Long notification_id) {
        return notificationRepository.findById(notification_id).orElse(null);
    }

    public Notification create(Notification notification) {
        return notificationRepository.save(notification);
    }

    public Notification update(Notification notification) {
        return notificationRepository.save(notification);
    }

    public void delete(Long notification_id) {
        notificationRepository.deleteById(notification_id);
    }
}
