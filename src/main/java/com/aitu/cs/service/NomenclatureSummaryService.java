package com.aitu.cs.service;

import com.aitu.cs.entity.NomenclatureSummary;
import com.aitu.cs.repository.NomenclatureSummaryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NomenclatureSummaryService {
    public final NomenclatureSummaryRepository nomenclatureSummaryRepository;

    public NomenclatureSummaryService(NomenclatureSummaryRepository nomenclatureSummaryRepository) {
        this.nomenclatureSummaryRepository = nomenclatureSummaryRepository;
    }
    public List<NomenclatureSummary> getAll() {
        return (List<NomenclatureSummary>) nomenclatureSummaryRepository.findAll();
    }

    public NomenclatureSummary  getById(Long nomenclatureSummary_id) {
        return nomenclatureSummaryRepository.findById(nomenclatureSummary_id).orElse(null);
    }

    public NomenclatureSummary create(NomenclatureSummary nomenclatureSummary) {
        return nomenclatureSummaryRepository.save(nomenclatureSummary);
    }

    public NomenclatureSummary update(NomenclatureSummary nomenclatureSummary) {
        return nomenclatureSummaryRepository.save(nomenclatureSummary);
    }

    public void delete(Long _id) {
        nomenclatureSummaryRepository.deleteById(_id);
    }
}
