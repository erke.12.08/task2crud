package com.aitu.cs.service;

import com.aitu.cs.entity.Nomenclature;
import com.aitu.cs.repository.NomenclatureRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NomenclatureService {
    public final NomenclatureRepository nomenclatureRepository;

    public NomenclatureService(NomenclatureRepository nomenclatureRepository) {
        this.nomenclatureRepository = nomenclatureRepository;
    }

    public List<Nomenclature> getAll() {
        return (List<Nomenclature>) nomenclatureRepository.findAll();
    }

    public Nomenclature getById(Long nomenclature_id) {
        return nomenclatureRepository.findById(nomenclature_id).orElse(null);
    }

    public Nomenclature create(Nomenclature nomenclature) {
        return nomenclatureRepository.save(nomenclature);
    }

    public Nomenclature update(Nomenclature nomenclature) {
        return nomenclatureRepository.save(nomenclature);
    }

    public void delete(Long nomenclature_id) {
        nomenclatureRepository.deleteById(nomenclature_id);
    }

}
