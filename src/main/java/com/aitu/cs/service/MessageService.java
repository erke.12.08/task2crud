package com.aitu.cs.service;


import com.aitu.cs.entity.Message;
import com.aitu.cs.repository.MessageRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageService {
    public final MessageRepository messageRepository;

    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }
    public List<Message> getAll() {
        return (List<Message>) messageRepository.findAll();
    }

    public Message getById(Long message_id) {
        return messageRepository.findById(message_id).orElse(null);
    }

    public Message create(Message message) {
        return messageRepository.save(message);
    }

    public Message update(Message message) {
        return messageRepository.save(message);
    }

    public void delete(Long message_id) {
        messageRepository.deleteById(message_id);
    }
}
