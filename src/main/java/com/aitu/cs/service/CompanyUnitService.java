package com.aitu.cs.service;

import com.aitu.cs.entity.CompanyUnit;
import com.aitu.cs.repository.CompanyUnitRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyUnitService {

    public final CompanyUnitRepository companyUnitRepository;

    public CompanyUnitService(CompanyUnitRepository companyUnitRepository) {

        this.companyUnitRepository = companyUnitRepository;
    }
    public List<CompanyUnit> getAll() {
        return (List<CompanyUnit>) companyUnitRepository.findAll();
    }

    public CompanyUnit getById(Long _id) {
        return companyUnitRepository.findById(_id).orElse(null);
    }

    public CompanyUnit create(CompanyUnit companyUnit) {
        return companyUnitRepository.save(companyUnit);
    }

    public CompanyUnit  update(CompanyUnit companyUnit) {
        return companyUnitRepository.save(companyUnit);
    }

    public void delete(Long companyUnit_id) {
        companyUnitRepository.deleteById(companyUnit_id);
    }


}
