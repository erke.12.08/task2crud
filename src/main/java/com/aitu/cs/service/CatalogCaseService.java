package com.aitu.cs.service;


import com.aitu.cs.entity.CatalogCase;
import com.aitu.cs.repository.CatalogCaseRepository;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogCaseService {
    private final CatalogCaseRepository catalogCaseRepository;

    public CatalogCaseService(CatalogCaseRepository catalogCaseRepository) {
        this.catalogCaseRepository = catalogCaseRepository;
    }
    public List<CatalogCase> getAll() {
        return (List<CatalogCase>) catalogCaseRepository.findAll();
    }

    public CatalogCase  getById(Long catalogCase_id) {
        return catalogCaseRepository.findById(catalogCase_id).orElse(null);
    }

    public CatalogCase create(CatalogCase catalogCase) {
        return catalogCaseRepository.save(catalogCase);
    }

    public CatalogCase update(CatalogCase catalogCase) {
        return catalogCaseRepository.save(catalogCase);
    }

    public void delete(Long catalog_id) {
        catalogCaseRepository.deleteById(catalog_id);
    }

}
