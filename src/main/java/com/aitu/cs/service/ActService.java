package com.aitu.cs.service;

import com.aitu.cs.entity.Act;
import com.aitu.cs.repository.ActRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActService {

    private final ActRepository actRepository;

    public ActService(ActRepository actRepository) {
        this.actRepository = actRepository;
    }
    public List<Act> getAll() {
        return (List<Act>) actRepository.findAll();
    }

    public Act getById(Long _id) {
        return actRepository.findById(_id).orElse(null);
    }

    public Act  create(Act act) {
        return actRepository.save(act);
    }

    public Act  update(Act act) {
        return actRepository.save(act);
    }

    public void delete(Long act_id) {
        actRepository.deleteById(act_id);
    }
}
