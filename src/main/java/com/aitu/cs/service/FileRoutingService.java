package com.aitu.cs.service;

import com.aitu.cs.entity.FileRouting;
import com.aitu.cs.repository.FileRoutingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileRoutingService {
    private final FileRoutingRepository fileRoutingRepository;

    public FileRoutingService(FileRoutingRepository fileRoutingRepository) {
        this.fileRoutingRepository = fileRoutingRepository;
    }
    public List<FileRouting> getAll() {
        return (List<FileRouting>) fileRoutingRepository.findAll();
    }

    public FileRouting getById(Long fileRouting_id) {
        return fileRoutingRepository.findById(fileRouting_id).orElse(null);
    }

    public FileRouting create(FileRouting fileRouting) {
        return fileRoutingRepository.save(fileRouting);
    }

    public FileRouting update(FileRouting fileRouting) {
        return fileRoutingRepository.save(fileRouting);
    }

    public void delete(Long fileRouting_id) {
        fileRoutingRepository.deleteById(fileRouting_id);
    }

}
