package com.aitu.cs.service;

import com.aitu.cs.entity.Fond;
import com.aitu.cs.repository.FondRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FondService {
    public final FondRepository fondRepository;

    public FondService(FondRepository fondRepository) {
        this.fondRepository = fondRepository;
    }

    public List<Fond> getAll() {
        return (List<Fond>) fondRepository.findAll();
    }

    public Fond getById(Long fond_id) {
        return fondRepository.findById(fond_id).orElse(null);
    }

    public Fond  create(Fond fond) {
        return fondRepository.save(fond);
    }

    public Fond update(Fond fond) {
        return fondRepository.save(fond);
    }

    public void delete(Long fond_id) {
        fondRepository.deleteById(fond_id);
    }


}
