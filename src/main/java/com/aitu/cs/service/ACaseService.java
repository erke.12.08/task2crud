package com.aitu.cs.service;


import com.aitu.cs.entity.ACase;
import com.aitu.cs.repository.ACaseRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ACaseService {
    public final ACaseRepository aCaseRepository;

    public ACaseService(ACaseRepository aCaseRepository) {
        this.aCaseRepository = aCaseRepository;
    }


    public List<ACase> getAll() {
        return (List<ACase>) aCaseRepository.findAll();
    }

    public ACase getById(Long aCase_id) {
        return aCaseRepository.findById(aCase_id).orElse(null);
    }

    public ACase create(ACase aCase) {
        return aCaseRepository.save(aCase);
    }

    public ACase update(ACase aCase) {
        return aCaseRepository.save(aCase);
    }

    public void delete(Long aCase_id) {
        aCaseRepository.deleteById(aCase_id);
    }
}
