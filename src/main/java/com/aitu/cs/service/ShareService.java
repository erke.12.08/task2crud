package com.aitu.cs.service;


import com.aitu.cs.entity.Share;
import com.aitu.cs.repository.ShareRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShareService {

    public final ShareRepository shareRepository;

    public ShareService(ShareRepository shareRepository) {
        this.shareRepository = shareRepository;
    }

    public List<Share> getAll() {
        return (List<Share>) shareRepository.findAll();
    }

    public Share getById(Long share_id) {
        return shareRepository.findById(share_id).orElse(null);
    }

    public Share create(Share share) {
        return shareRepository.save(share);
    }

    public Share update(Share share) {
        return shareRepository.save(share);
    }

    public void delete(Long share_id) {
        shareRepository.deleteById(share_id);
    }
}
