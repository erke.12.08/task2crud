package com.aitu.cs.service;

import com.aitu.cs.entity.ActivityJournal;
import com.aitu.cs.repository.ActivityJournalRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityJournalService {

    private final ActivityJournalRepository activityJournalRepository;

    public ActivityJournalService(ActivityJournalRepository activityJournalRepository) {
        this.activityJournalRepository = activityJournalRepository;
    }

    public List<ActivityJournal> getAll() {
        return (List<ActivityJournal>) activityJournalRepository.findAll();
    }

    public ActivityJournal getById(Long _id) {
        return activityJournalRepository.findById(_id).orElse(null);
    }

    public ActivityJournal create(ActivityJournal activityJournal) {
        return activityJournalRepository.save(activityJournal);
    }

    public ActivityJournal update(ActivityJournal activityJournal) {
        return activityJournalRepository.save(activityJournal);
    }

    public void delete(Long activityJournal_id) {
        activityJournalRepository.deleteById(activityJournal_id);
    }

}
