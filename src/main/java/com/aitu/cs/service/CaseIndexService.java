package com.aitu.cs.service;

import com.aitu.cs.entity.CaseIndex;
import com.aitu.cs.repository.CaseIndexRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaseIndexService {
    public final CaseIndexRepository caseIndexRepository;

    public CaseIndexService(CaseIndexRepository caseIndexRepository) {
        this.caseIndexRepository = caseIndexRepository;
    }

    public List<CaseIndex> getAll() {
        return (List<CaseIndex>) caseIndexRepository.findAll();
    }

    public CaseIndex getById(Long caseIndex_id) {
        return caseIndexRepository.findById(caseIndex_id).orElse(null);
    }

    public CaseIndex  create(CaseIndex caseIndex) {
        return caseIndexRepository.save(caseIndex);
    }

    public CaseIndex update(CaseIndex caseIndex) {
        return caseIndexRepository.save(caseIndex);
    }

    public void delete(Long caseIndex_id) {
        caseIndexRepository.deleteById(caseIndex_id);
    }

}
