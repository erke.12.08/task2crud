
drop table if exists history;


create table history (
	id serial not null
		constraint history_pk
			unique,
	request_id bigint,
	status VARCHAR(64),
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint
);

alter table history owner to postgres;

insert into history (id, request_id, status, created_timestamp, created_by, updated_timestamp, updated_by) values (1, 1, 'Indigo', 1600522898, 75, 1600521473, 11);
insert into history (id, request_id, status, created_timestamp, created_by, updated_timestamp, updated_by) values (2, 2, 'Orange', 1600522832, 33, 1600521476, 14);
insert into history (id, request_id, status, created_timestamp, created_by, updated_timestamp, updated_by) values (3, 3, 'Maroon', 1600522846, 74, 1600521472, 11);
insert into history (id, request_id, status, created_timestamp, created_by, updated_timestamp, updated_by) values (4, 4, 'Indigo', 1600522842, 49, 1600521470, 11);
insert into history (id, request_id, status, created_timestamp, created_by, updated_timestamp, updated_by) values (5, 5, 'Indigo', 1600522825, 51, 1600521475, 5);