drop table if exists nomen;


create table nomen (
	id serial not null
		constraint nomen_pk
			unique,
	nomenclature_number VARCHAR(128),
	year INT,
	nomenclature_summary_id bigint,
	company_unit_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint
);
alter table nomen owner to postgres;
insert into nomen (id, nomenclature_number, year, nomenclature_summary_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (1, '0.6.2', 2019, 15, 79, 1600528885, 65, 1600528559, 14);
insert into nomen (id, nomenclature_number, year, nomenclature_summary_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (2, '6.03', 2009, 10, 70, 1600528865, 24, 1600528409, 16);
insert into nomen (id, nomenclature_number, year, nomenclature_summary_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (3, '0.4.6', 2020, 17, 52, 1600528804, 44, 1600528284, 54);
insert into nomen (id, nomenclature_number, year, nomenclature_summary_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (4, '0.34', 2006, 7, 83, 1600528820, 88, 1600528014, 94);
insert into nomen (id, nomenclature_number, year, nomenclature_summary_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (5, '8.2.5', 2016, 15, 35, 1600528811, 82, 1600528771, 27);