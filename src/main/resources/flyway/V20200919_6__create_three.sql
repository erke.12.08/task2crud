DROP TABLE IF EXISTS users;


create table users (
	id serial not null
		constraint users_pk
			unique,
	auth_id INT,
	name VARCHAR(128),
	fullName VARCHAR(128),
	surname VARCHAR(128),
	secondName VARCHAR(128),
	status VARCHAR(128),
	company_unit_id bigint,
	password VARCHAR(128),
	last_login_timestamp bigint,
	IIN VARCHAR(32),
	is_active boolean,
	is_activated boolean,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint
);

alter table groups owner to postgres;