drop table if exists key;
create table key (
	id serial not null
		constraint key_pk
			unique,
	name VARCHAR(128),
	company_unit_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint
);

alter table key owner to postgres;

insert into key (id, name, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (1, 'vitae', 42, 1600528865, 4, 1600528941, 44);
insert into key (id, name, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (2, 'orci', 56, 1600528817, 54, 1600528164, 16);
insert into key (id, name, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (3, 'et', 3, 1600528847, 57, 1600528090, 90);
insert into key (id, name, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (4, 'metus', 14, 1600528864, 21, 1600528582, 52);
insert into key (id, name, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (5, 'quis', 38, 1600528887, 28, 1600528265, 10);