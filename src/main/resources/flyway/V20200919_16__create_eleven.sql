drop table if exists record;

create table record (
	id serial not null
		constraint record_pk
			unique,
	number VARCHAR(128),
	type VARCHAR(128),
	company_unit_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint
);

alter table record owner to postgres;

insert into record (id, number, type, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (1, '0.10', 'et', 64, 1600528856, 9, 1600528613, 13);
insert into record (id, number, type, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (2, '5.8', 'ridiculus', 47, 1600528848, 50, 1600528482, 28);
insert into record (id, number, type, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (3, '2.5.3', 'odio', 38, 1600528866, 96, 1600528665, 19);
insert into record (id, number, type, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (4, '5.4', 'sociis', 30, 1600528850, 29, 1600528165, 15);
insert into record (id, number, type, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (5, '8.2.1', 'nulla', 95, 1600528830, 51, 1600528900, 73);