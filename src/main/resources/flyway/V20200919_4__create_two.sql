DROP TABLE IF EXISTS acase;


create table acase (
	id serial not null
		constraint case_pk
			unique,
	case_number VARCHAR(128),
	volume_number VARCHAR(128),
	title_kz VARCHAR(128),
	title_ru VARCHAR(128),
	title_en VARCHAR(128),
	start_date bigint,
	finish_date bigint,
	pages_count bigint,
	edsTrue boolean,
	eds VARCHAR(50),
	sendNAG VARCHAR(50),
	delete boolean,
	limitedAccess VARCHAR(50),
	hash VARCHAR(128),
	version int,
	version_id varchar(128),
	is_active_version boolean,
	note varchar(255) ,
	location_id bigint,
	case_index_id bigint,
	record_id bigint,
	destroy_id bigint,
	company_unit_id bigint,
	case_address_block VARCHAR(128),
	date_input bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint
);
alter table groups owner to postgres;