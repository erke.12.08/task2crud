drop table if exists fond;

create table fond (
	id serial not null
		constraint fond_pk
			unique,
	fond_number VARCHAR(128),
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint
);
alter table fond owner to postgres;

insert into fond (id, fond_number, created_timestamp, created_by, updated_timestamp, updated_by) values (1, '0.2.7', 1600528815, 91, 1600528826, 36);
insert into fond (id, fond_number, created_timestamp, created_by, updated_timestamp, updated_by) values (2, '5.02', 1600528805, 16, 1600528723, 18);
insert into fond (id, fond_number, created_timestamp, created_by, updated_timestamp, updated_by) values (3, '0.3.8', 1600528802, 46, 1600528864, 80);
insert into fond (id, fond_number, created_timestamp, created_by, updated_timestamp, updated_by) values (4, '7.1.2', 1600528800, 41, 1600528892, 34);
insert into fond (id, fond_number, created_timestamp, created_by, updated_timestamp, updated_by) values (5, '0.7.4', 1600528848, 67, 1600528888, 21);