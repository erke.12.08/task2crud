drop table if exists catalogcase;

create table catalogcase (
id serial not null
		constraint catcase_pk
			unique,
	case_id bigint,
	catalog_id bigint,
	company_unit_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint
);

alter table catalogcase owner to postgres;

insert into catalogcase (id, case_id, catalog_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (1, 64, 75, 83, 1600528843, 68, 1600528560, 67);
insert into catalogcase (id, case_id, catalog_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (2, 88, 29, 29, 1600528858, 4, 1600528466, 42);
insert into catalogcase (id, case_id, catalog_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (3, 91, 15, 42, 1600528886, 51, 1600528736, 19);
insert into catalogcase (id, case_id, catalog_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (4, 7, 87, 27, 1600528862, 68, 1600528294, 38);
insert into catalogcase (id, case_id, catalog_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (5, 38, 55, 83, 1600528879, 29, 1600528937, 1);