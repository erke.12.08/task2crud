drop table if exists activity;

create table activity (
	id serial not null
		constraint activ_pk
			unique,
	activity_type VARCHAR(128),
	object_type VARCHAR(256),
	object_id bigint,
	created_timestamp bigint,
	created_by bigint,
	message_level VARCHAR(128),
	message VARCHAR(128)
);

alter table activity owner to postgres;
insert into activity (id, activity_type, object_type, object_id, created_timestamp, created_by, message_level, message) values (1, 'Basic Industries', 'application/x-vnd.ls-xpix', 98, 1600528838, 6, 'Maroon', 'ipsum dolor');
insert into activity (id, activity_type, object_type, object_id, created_timestamp, created_by, message_level, message) values (2, 'n/a', 'image/pjpeg', 79, 1600528844, 4, 'Red', 'primis in');
insert into activity (id, activity_type, object_type, object_id, created_timestamp, created_by, message_level, message) values (3, 'Energy', 'image/cmu-raster', 16, 1600528812, 2, 'Goldenrod', 'ac neque duis bibendum');
insert into activity (id, activity_type, object_type, object_id, created_timestamp, created_by, message_level, message) values (4, 'Finance', 'application/octet-stream', 92, 1600528889, 3, 'Purple', 'libero rutrum ac lobortis vel');
insert into activity (id, activity_type, object_type, object_id, created_timestamp, created_by, message_level, message) values (5, 'Technology', 'video/x-ms-asf', 20, 1600528829, 1, 'Yellow', 'curabitur gravida');