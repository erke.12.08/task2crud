drop table if exists unit;

create table unit (
	id serial not null
		constraint unit_pk
			unique,
	name_ru VARCHAR(128),
	name_kz VARCHAR(128),
	name_en VARCHAR(128),
	parent_id bigint,
	year INT,
	company_id INT,
	code_index VARCHAR(16),
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint
);

alter table unit owner to postgres;

insert into unit (id, name_ru, name_kz, name_en, parent_id, year, company_id, code_index, created_timestamp, created_by, updated_timestamp, updated_by) values (1, 'dapibus nulla', 'nulla sed', 'dapibus at', 90, 2011, 22, '0.7.9', 1600528832, 48, 1600528600, 28);
insert into unit (id, name_ru, name_kz, name_en, parent_id, year, company_id, code_index, created_timestamp, created_by, updated_timestamp, updated_by) values (2, 'duis aliquam', 'quam turpis', 'justo etiam pretium iaculis', 23, 2007, 20, '0.73', 1600528859, 51, 1600528288, 64);
insert into unit (id, name_ru, name_kz, name_en, parent_id, year, company_id, code_index, created_timestamp, created_by, updated_timestamp, updated_by) values (3, 'ligula nec sem', 'eleifend pede libero', 'nulla suscipit ligula', 60, 2018, 78, '7.8', 1600528810, 81, 1600528279, 66);
insert into unit (id, name_ru, name_kz, name_en, parent_id, year, company_id, code_index, created_timestamp, created_by, updated_timestamp, updated_by) values (4, 'quis odio consequat varius', 'in eleifend quam a', 'penatibus et magnis dis', 2, 2016, 4, '6.37', 1600528836, 3, 1600528985, 74);
insert into unit (id, name_ru, name_kz, name_en, parent_id, year, company_id, code_index, created_timestamp, created_by, updated_timestamp, updated_by) values (5, 'in quis justo', 'aliquam convallis nunc proin', 'nam nulla integer pede', 88, 2006, 59, '5.90', 1600528829, 33, 1600528680, 31);