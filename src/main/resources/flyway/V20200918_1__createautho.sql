DROP TABLE IF EXISTS groups;
create table groups (
	id serial not null
		constraint group_pk
			unique,
	name varchar default 255
);

alter table groups owner to postgres;