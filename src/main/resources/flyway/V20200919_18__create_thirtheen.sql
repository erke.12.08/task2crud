drop table if exists file;

create table file (
	id serial not null
		constraint file_pk
			unique,
	name VARCHAR(128),
	type VARCHAR(128),
	size bigint,
	page_count INT,
	hash VARCHAR(128),
	is_deleted boolean,
	file_binary_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint
);
alter table file owner to postgres;

insert into file (id, name, type, size, page_count, hash, is_deleted, file_binary_id, created_timestamp, created_by, updated_timestamp, updated_by) values (1, 'fusce', 'text/plain', 42, 70, 'adipiscing', false, 81, 1600528860, 32, 1600528070, 84);
insert into file (id, name, type, size, page_count, hash, is_deleted, file_binary_id, created_timestamp, created_by, updated_timestamp, updated_by) values (2, 'magna', 'text/plain', 30, 48, 'volutpat', true, 58, 1600528824, 18, 1600528762, 80);
insert into file (id, name, type, size, page_count, hash, is_deleted, file_binary_id, created_timestamp, created_by, updated_timestamp, updated_by) values (3, 'pellentesque', 'text/plain', 28, 67, 'erat', false, 47, 1600528828, 27, 1600528892, 13);
insert into file (id, name, type, size, page_count, hash, is_deleted, file_binary_id, created_timestamp, created_by, updated_timestamp, updated_by) values (4, 'neque', 'text/plain', 68, 74, 'dui', true, 10, 1600528811, 21, 1600528988, 97);
insert into file (id, name, type, size, page_count, hash, is_deleted, file_binary_id, created_timestamp, created_by, updated_timestamp, updated_by) values (5, 'hac', 'text/plain', 49, 42, 'nisi', false, 15, 1600528806, 73, 1600528300, 81);