drop table if exists catalog;

create table catalog (
	id serial not null
		constraint cat_pk
			unique,
	name_ru VARCHAR(128),
	name_kz VARCHAR(128),
	name_en VARCHAR(128),
	parent_id bigint,
	company_unit_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint
);
alter table catalog owner to postgres;

insert into catalog (id, name_ru, name_kz, name_en, parent_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (1, 'at feugiat', 'nec dui', 'lacinia', 85, 1, 1600528871, 19, 1600528696, 3);
insert into catalog (id, name_ru, name_kz, name_en, parent_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (2, 'nullam orci', 'vitae nisl', 'velit', 11, 10, 1600528801, 100, 1600528448, 65);
insert into catalog (id, name_ru, name_kz, name_en, parent_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (3, 'pellentesque', 'dapibus duis', 'condimentum id', 48, 57, 1600528831, 33, 1600528047, 88);
insert into catalog (id, name_ru, name_kz, name_en, parent_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (4, 'vestibulum', 'duis', 'turpis', 25, 65, 1600528857, 4, 1600528747, 31);
insert into catalog (id, name_ru, name_kz, name_en, parent_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (5, 'eros', 'mattis pulvinar', 'orci', 13, 8, 1600528865, 81, 1600528844, 77);