DROP TABLE IF EXISTS request;

create table request (
	id serial not null
		constraint request_pk
			unique,
	request_user_id bigint,
	response_user_id bigint,
	case_id bigint,
	case_index_id bigint,
	created_type VARCHAR(64),
	comment VARCHAR(255),
	status VARCHAR(64),
	timestamp bigint,
	sharestart bigint,
	sharefinish bigint,
	favorite boolean,
	update_timestamp bigint,
	update_by bigint,
	declinenote VARCHAR(255),
	company_unit_id bigint,
	from_request_id bigint
);

alter table users owner to postgres;
alter table request owner to postgres;