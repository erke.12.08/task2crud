drop table if exists index;

create table index (
	id serial not null
		constraint index_pk
			unique,
	case_index VARCHAR(128),
	title_ru VARCHAR(128),
	title_kz VARCHAR(128),
	title_en VARCHAR(128),
	storage_type INT,
	storage_year INT,
	note VARCHAR(128),
	company_unit_id bigint,
	nomenclature_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint
);

alter table index owner to postgres;

insert into index (id, case_index, title_ru, title_kz, title_en, storage_type, storage_year, note, company_unit_id, nomenclature_id, created_timestamp, created_by, updated_timestamp, updated_by) values (1, 'XzgeuqneET5', 'arcu sed augue aliquam', 'posuere cubilia curae nulla dapibus', 'viverra pede ac diam', 3, 2009, 'praesent', 14, 4, 1600524199, 6, 1600524118, 2);
insert into index (id, case_index, title_ru, title_kz, title_en, storage_type, storage_year, note, company_unit_id, nomenclature_id, created_timestamp, created_by, updated_timestamp, updated_by) values (2, 'b8Yxcx3L', 'donec vitae nisi', 'suspendisse ornare consequat', 'sit amet consectetuer adipiscing elit', 6, 2006, 'lobortis est phasellus sit', 10, 11, 1600524191, 5, 1600524118, 7);
insert into index (id, case_index, title_ru, title_kz, title_en, storage_type, storage_year, note, company_unit_id, nomenclature_id, created_timestamp, created_by, updated_timestamp, updated_by) values (3, 'beQna1wg', 'interdum eu tincidunt in', 'congue etiam justo etiam', 'magnis dis parturient montes', 5, 2008, 'neque', 11, 4, 1600524193, 3, 1600524172, 14);
insert into index (id, case_index, title_ru, title_kz, title_en, storage_type, storage_year, note, company_unit_id, nomenclature_id, created_timestamp, created_by, updated_timestamp, updated_by) values (4, 'W4DsFihDkIJ', 'luctus et ultrices posuere', 'dui maecenas', 'diam nam tristique tortor', 2, 2008, 'quis turpis sed ante vivamus', 2, 12, 1600524193, 14, 1600524103, 1);
insert into index (id, case_index, title_ru, title_kz, title_en, storage_type, storage_year, note, company_unit_id, nomenclature_id, created_timestamp, created_by, updated_timestamp, updated_by) values (5, 'rD3Ol0U', 'lorem ipsum', 'nam tristique tortor eu pede', 'sit amet eros', 5, 1995, 'a', 3, 9, 1600524190, 9, 1600524115, 6);