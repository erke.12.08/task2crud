drop table if exists act;

create table act (
	id serial not null
		constraint act_pk
			unique,
	act_number VARCHAR(128),
	base VARCHAR(256),
	company_unit_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint
);
alter table act owner to postgres;
insert into act (id, act_number, base, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (1, '4.20', 'non', 70, 1600528817, 14, 1600528424, 19);
insert into act (id, act_number, base, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (2, '2.0.1', 'magna', 88, 1600528806, 100, 1600528550, 56);
insert into act (id, act_number, base, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (3, '0.26', 'at', 100, 1600528870, 48, 1600528323, 8);
insert into act (id, act_number, base, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (4, '0.83', 'mauris', 64, 1600528828, 32, 1600528314, 65);
insert into act (id, act_number, base, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (5, '0.6.7', 'porta', 36, 1600528812, 80, 1600528948, 44);