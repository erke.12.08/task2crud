drop table if exists keyrouting;

create table keyrouting (
	id serial not null
		constraint kr_pk
			unique,
	search_key_id bigint,
	table_name VARCHAR(128),
	table_id bigint,
	type VARCHAR(128)
);

alter table keyrouting owner to postgres;
insert into keyrouting (id, search_key_id, table_name, table_id, type) values (1, 47, 'nibh', 116, 'nisl');
insert into keyrouting (id, search_key_id, table_name, table_id, type) values (2, 97, 'habitasse', 173, 'nulla');
insert into keyrouting (id, search_key_id, table_name, table_id, type) values (3, 42, 'massa', 186, 'lorem');
insert into keyrouting (id, search_key_id, table_name, table_id, type) values (4, 46, 'sem', 38, 'pellentesque ultrices');
insert into keyrouting (id, search_key_id, table_name, table_id, type) values (5, 1, 'ultrices', 178, 'ante');