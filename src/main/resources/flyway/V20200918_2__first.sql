DROP TABLE IF EXISTS auth;

create table auth(
id serial not null
constraint auth_pk unique ,
username varchar(255),
email varchar (255),
password varchar (128),
role varchar (255),
forgot_password_key varchar (128),
forgot_password_key_timestamp bigint ,
company_unit_id bigint
);

alter table auth owner to postgres;