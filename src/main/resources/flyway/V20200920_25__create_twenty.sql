drop table if exists notif;

create table notif (
	id serial not null
		constraint not_pk
			unique,
	object_type VARCHAR(128),
	object_id bigint,
	company_unit_id bigint,
	user_id bigint,
	created_timestamp bigint,
	viewed_timestamp bigint,
	is_viewed boolean,
	title VARCHAR(255),
	text VARCHAR(255),
	company_id bigint
);

alter table notif owner to postgres;
insert into notif (id, object_type, object_id, company_unit_id, user_id, created_timestamp, viewed_timestamp, is_viewed, title, text, company_id) values (1, 'application/lha', 34, 91, 64, 1600528804, 1600528254, false, 'sapien dignissim', 'risus auctor', 8);
insert into notif (id, object_type, object_id, company_unit_id, user_id, created_timestamp, viewed_timestamp, is_viewed, title, text, company_id) values (2, 'text/webviewhtml', 51, 42, 40, 1600528850, 1600528679, true, 'hac', 'platea dictumst', 45);
insert into notif (id, object_type, object_id, company_unit_id, user_id, created_timestamp, viewed_timestamp, is_viewed, title, text, company_id) values (3, 'video/quicktime', 78, 91, 14, 1600528870, 1600528493, false, 'non', 'pellentesque ultrices mattis odio donec', 97);
insert into notif (id, object_type, object_id, company_unit_id, user_id, created_timestamp, viewed_timestamp, is_viewed, title, text, company_id) values (4, 'application/wordperfect6.1', 96, 89, 4, 1600528836, 1600528027, true, 'ultrices', 'aliquam sit', 33);
insert into notif (id, object_type, object_id, company_unit_id, user_id, created_timestamp, viewed_timestamp, is_viewed, title, text, company_id) values (5, 'application/x-excel', 39, 76, 46, 1600528853, 1600528684, true, 'sapien a', 'vivamus tortor duis mattis', 100);