drop table if exists location;

create table location (
	id serial not null
		constraint loc_pk
			unique,
	row VARCHAR(64),
	line VARCHAR(64),
	column_name VARCHAR(64),
	box VARCHAR(64),
	company_unit_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint
);

alter table location owner to postgres;

insert into location (id, row, line, column_name, box, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (1, 'cum', 'mauris', 'vivamus in', 'id', 60, 1600528809, 45, 1600528757, 99);
insert into location (id, row, line, column_name, box, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (2, 'leo pellentesque', 'diam', 'ut', 'varius', 61, 1600528843, 30, 1600528875, 97);
insert into location (id, row, line, column_name, box, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (3, 'nibh in', 'turpis', 'turpis', 'congue', 2, 1600528878, 79, 1600528101, 63);
insert into location (id, row, line, column_name, box, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (4, 'sit', 'nulla neque', 'natoque penatibus', 'nulla', 14, 1600528861, 40, 1600528007, 77);
insert into location (id, row, line, column_name, box, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (5, 'nulla pede', 'in', 'sapien ut', 'ligula', 51, 1600528819, 90, 1600528904, 86);