drop table if exists sum;

create table sum (
	id serial not null
		constraint sum_pk
			unique,
	number VARCHAR(128),
	year INT,
	company_unit_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint
);

alter table sum owner to postgres;
insert into sum (id, number, year, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (1, '0.52', 2019, 23, 1600528809, 16, 1600528807, 54);
insert into sum (id, number, year, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (2, '0.8.5', 2000, 65, 1600528809, 35, 1600528218, 71);
insert into sum (id, number, year, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (3, '5.15', 2008, 64, 1600528826, 88, 1600528578, 75);
insert into sum (id, number, year, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (4, '3.1', 2002, 54, 1600528849, 90, 1600528872, 34);
insert into sum (id, number, year, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by) values (5, '5.4.0', 2004, 82, 1600528847, 89, 1600528837, 11);