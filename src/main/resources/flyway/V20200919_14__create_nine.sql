
drop table if exists company;

create table company (
	id serial not null
		constraint company_pk
			unique,
	name_ru VARCHAR(128),
	name_kz VARCHAR(128),
	name_en VARCHAR(128),
	bin VARCHAR(32),
	parent_id bigint,
	fond_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint
);

alter table company owner to postgres;

insert into company (id, name_ru, name_kz, name_en, bin, parent_id, fond_id, created_timestamp, created_by, updated_timestamp, updated_by) values (1, 'eu felis fusce posuere', 'quis odio', 'turpis nec', 'KZ331127384690557', 16, 17, 1600528845, 21, 1600528218, 96);
insert into company (id, name_ru, name_kz, name_en, bin, parent_id, fond_id, created_timestamp, created_by, updated_timestamp, updated_by) values (2, 'et eros vestibulum', 'auctor sed', 'maecenas leo', 'KZ502899839330893', 12, 100, 1600528886, 14, 1600528889, 57);
insert into company (id, name_ru, name_kz, name_en, bin, parent_id, fond_id, created_timestamp, created_by, updated_timestamp, updated_by) values (3, 'morbi a ipsum', 'sagittis sapien cum', 'in hac', 'KZ186528486206601', 11, 46, 1600528871, 71, 1600528661, 60);
insert into company (id, name_ru, name_kz, name_en, bin, parent_id, fond_id, created_timestamp, created_by, updated_timestamp, updated_by) values (4, 'sollicitudin vitae consectetuer', 'dui proin', 'risus semper porta volutpat', 'KZ984404982362862', 7, 76, 1600528811, 55, 1600528519, 60);
insert into company (id, name_ru, name_kz, name_en, bin, parent_id, fond_id, created_timestamp, created_by, updated_timestamp, updated_by) values (5, 'varius ut', 'eget tempus vel pede', 'rutrum nulla tellus', 'KZ466022806335427', 16, 4, 1600528811, 62, 1600528640, 93);