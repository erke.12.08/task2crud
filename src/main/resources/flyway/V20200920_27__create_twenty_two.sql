
drop table if exists route;
create table route (
	id serial not null
		constraint route_pk
			unique,
	file_id bigint,
	table_name VARCHAR(128),
	table_id bigint,
	type VARCHAR(128)
);
alter table route owner to postgres;
insert into route (id, file_id, table_name, table_id, type) values (1, 97, 'aliquam', 9, 'amet consectetuer');
insert into route (id, file_id, table_name, table_id, type) values (2, 97, 'luctus', 207, 'pellentesque');
insert into route (id, file_id, table_name, table_id, type) values (3, 2, 'dis', 14, 'turpis elementum');
insert into route (id, file_id, table_name, table_id, type) values (4, 33, 'ipsum', 102, 'nunc');
insert into route (id, file_id, table_name, table_id, type) values (5, 15, 'vestibulum', 192, 'pede');