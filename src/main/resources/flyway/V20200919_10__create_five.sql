DROP TABLE IF EXISTS share;

create table share (
	id serial not null
		constraint share_pk
			unique,
	request_id bigint,
	note VARCHAR(255),
	sender_id bigint,
	receiver_id bigint,
	share_timestamp bigint
);

alter table share owner to postgres;

insert into share (id, request_id, note, sender_id, receiver_id, share_timestamp) values (1, 1, 'Mauv', 38, 16, 1600521473);
insert into share (id, request_id, note, sender_id, receiver_id, share_timestamp) values (2, 2, 'Yellow', 77, 53, 1600521477);
insert into share (id, request_id, note, sender_id, receiver_id, share_timestamp) values (3, 3, 'Pink', 12, 34, 1600521475);
insert into share (id, request_id, note, sender_id, receiver_id, share_timestamp) values (4, 4, 'Orange', 92, 15, 1600521470);
insert into share (id, request_id, note, sender_id, receiver_id, share_timestamp) values (5, 5, 'Red', 43, 54, 1600521474);